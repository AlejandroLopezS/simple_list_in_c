/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _SimpleList_H
#define _SimpleList_H

#include <stdlib.h>
#include "Node.h"

typedef struct {
    int   number;
    Node* first;
} SimpleList;

SimpleList SimpleListNULL = {0, NULL};

SimpleList* newSimpleList(int number, Node* first);

void freeSimpleList(SimpleList* list);

int getNumber(SimpleList list);

Node* getFirst(SimpleList list);

#include "SimpleList.c"
#endif
