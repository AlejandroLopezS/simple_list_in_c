/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _Node_C
#define _Node_C

#include "Node.h"

Node* newNode(char item, Node* next) {
    Node* node = (Node*) malloc(sizeof(Node));
    node->item = item;
    node->next = next;
    return node;
}

void freeNode(Node* node) {
    if (node != NULL) {
        freeNode(node->next);
        free(node);
    }
}

char getItem(Node node) {
	return node.item;
}

Node* getNext(Node node) {
	return node.next;
}

#endif
