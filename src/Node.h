/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _Node_H
#define _Node_H

#include <stdlib.h>

typedef struct Node {
    char         item;
    struct Node* next;
} Node;

Node NodeNULL = {'\0', NULL};

Node* newNode(char item, Node* next);

void freeNode(Node* node);

char getItem(Node node);

Node* getNext(Node node);

#include "Node.c"
#endif
