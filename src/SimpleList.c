/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _SimpleList_C
#define _SimpleList_C

#include "SimpleList.h"

SimpleList* newSimpleList(int number, Node* first) {
    SimpleList* list = (SimpleList*) malloc(sizeof(SimpleList));
    list->number = number;
    list->first  = first;
    return list;
}

void freeSimpleList(SimpleList* list) {
    if (list != NULL) {
        freeNode(list->first);
        free(list);
    }
}

int getNumber(SimpleList list) {
	return list.number;
}

Node* getFirst(SimpleList list) {
	return list.first;
}

#endif
