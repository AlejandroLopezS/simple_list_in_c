#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "SimpleList.h"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

int indexOf(char value, SimpleList list) {
    /*if(value == 'B'){
       return 0; 
    }
    */
    int k=0;
    for(Node* current = list.first; current != NULL; current = current -> next){
      if(current->item == value){
         return k;
      }
      else{
        k++;
      }
    }
    /*
     Node* current = list.first;
    for(int k = 0; k<list.number; k++){
        if(current->item==value){
            return k;
        }
        current = current->next;
    }
    */
    return -1;
    
}

char valueAtIndex(int index, SimpleList list) {
    int k=0;
    for(Node* current = list.first; current!=NULL; current=current->next){
        if(k == index){
            return current->item;
        }
    k++;
    }
    return '!';
}

SimpleList addAtFirst(char value, SimpleList list) {
    list.first = newNode('#',NULL);
    list.number = 1;
    return list;
}

SimpleList removeAtFirst(SimpleList list) {
    list.first = newNode('#',NULL);
    list.number = 1;
    return list;
}

SimpleList addAtLast(char value, SimpleList list) {
    list.first = newNode('#',NULL);
    list.number = 1;
    return list;
}

SimpleList removeAtLast(SimpleList list) {
    list.first = newNode('#',NULL);
    list.number = 1;
    return list;
}

SimpleList addAtIndex(char value, int index, SimpleList list) {
    list.first = newNode('#',NULL);
    list.number = 1;
    return list;
}

SimpleList removeAtIndex(int index, SimpleList list) {
    list.first = newNode('#',NULL);
    list.number = 1;
    return list;
}

SimpleList removeValue(char value, SimpleList list) {
    list.first = newNode('#',NULL);
    list.number = 1;
    return list;
}
