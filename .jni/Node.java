public class Node {
    private char item;
    private Node next;

    @executerpane.ConstructorAnnotation(signature = "NodeNULL")
    public Node() {
        this.item = '\0';
        this.next = null;
    }
    
    @executerpane.ConstructorAnnotation(signature = "Node(char[])")
    public Node(char... value) {
        this.next = null;
        if (value.length == 0) {
            item = '\0';
        } else {
        	item = value[0];
        	Node last = this;
        	for (int k=1; k<value.length; k++) {
        		last.next = new Node(value[k], null);
        		last = last.next;
        	}
        }
    }
    
    private Node(char item, Node next) {
        this.item = item;
        this.next = next;
    }
    
    public void addAtFirst(char item) {
    	this.next = new Node(this.item, next);
    	this.item = item;
    }
    
    public void addAtLast(char item) {
    	Node last = this;
    	while (last.next != null) {
    		last = last.next;
    	}
    	last.next = new Node(item, null);
    }
    
    @Override
	public String toString() {
		return "|'" + item + "'|\u2192" + next;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Node other = (Node) obj;
		if (item != other.item) {
			return false;
		}
		if (next == other.next) {
			return true;
		} else if (next == null) {
			return false;
		} else {
			return next.equals(other.next);
		}		
	}
}