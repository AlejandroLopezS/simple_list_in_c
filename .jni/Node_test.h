/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _Node_test_h
#define _Node_test_h

#define assertEquals_Node(expected, actual)                                                                       if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoNode_(__FILE__, __FUNCTION__, __LINE__);assertEquals_Node_(expected, actual)
#define assertArrayEquals_Node(expected, expectedLength, actual, actualLength)                                    if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoNode_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_Node_(expected, expectedLength, actual, actualLength)
#define assertMatrixEquals_Node(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)       if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoNode_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_Node_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_Node(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns) if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoNode_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_Node_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)

#include "Node_test.c"
#endif
