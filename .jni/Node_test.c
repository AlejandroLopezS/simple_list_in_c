/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _Node_test_c
#define _Node_test_c

#include "Node.h"
#include "Node_test.h"
#include "jni_test.h"

extern JNIEnv *javaEnv;

//////////////////////////////////////////////////////////
//         ObjectToStruct and StructToObject            //
//////////////////////////////////////////////////////////

jobject toJNodePtr
  (Node* value)
{
    if (value == NULL) {
        return NULL;

    } else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "LNode;");
		jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(CLNode;)V");
		return (*javaEnv)->NewObject(javaEnv, cls, constructor, toJchar(value->item), toJNodePtr(value->next));
    }
}

jobject toJNode
  (Node value)
{
    jclass cls = (*javaEnv)->FindClass(javaEnv, "LNode;");
    jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "(CLNode;)V");
    return (*javaEnv)->NewObject(javaEnv, cls, constructor, toJchar(value.item), toJNodePtr(value.next));
}

Node* toNodePtr
  (jobject value)
{
    if ((*javaEnv)->IsSameObject(javaEnv, value, NULL)) {
    	return NULL;

    } else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "LNode;");

		jfieldID itemFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "item", "C");
		jchar c = (*javaEnv)->GetCharField(javaEnv, value, itemFieldID);

		jfieldID nextFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "next", "LNode;");
		jobject next = (*javaEnv)->GetObjectField(javaEnv, value, nextFieldID);

		Node* c_value = (Node*) calloc(sizeof(Node), 1);
		c_value->item = toChar(c);
		c_value->next = toNodePtr(next);
		return c_value;
    }
}

Node toNode
  (jobject value)
{
    if ((*javaEnv)->IsSameObject(javaEnv, value, NULL)) {
        return NodeNULL;

    } else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "LNode;");

		jfieldID itemFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "item", "C");
		jchar c = (*javaEnv)->GetCharField(javaEnv, value, itemFieldID);

		jfieldID nextFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "next", "LNode;");
		jobject next = (*javaEnv)->GetObjectField(javaEnv, value, nextFieldID);

		Node c_value;
		c_value.item = toChar(c);
		c_value.next = toNodePtr(next);
		return c_value;
    }
}

void setNodeValue
  (jobject object, Node* value)
{
    if (value != NULL && ! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
        jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);

        jfieldID itemFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "item", "C");
        (*javaEnv)->SetIntField(javaEnv, object, itemFieldID, value->item);

        jfieldID nextFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "next", "LNode;");
        (*javaEnv)->SetObjectField(javaEnv, object, nextFieldID, toJNodePtr(value->next));
    }
}


//////////////////////////////////////////////////////////
//                 Common operations                    //
//////////////////////////////////////////////////////////

jobjectArray toJNodeArray
  (Node* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "LNode;"), NULL);
	} else {
		jobjectArray outArray = (*javaEnv)->NewObjectArray(javaEnv, *arrayLength, (*javaEnv)->FindClass(javaEnv, "LNode;"), NULL);
		jobject Node;
		for (int k=0; k < *arrayLength; k++) {
			Node = toJNode(arrayValue[k]);
			(*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, Node);
		}
		return outArray;
	}
}

jobjectArray toJNodeMatrix
  (Node** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "[LNode;"), NULL);
	} else {
		jobjectArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv, "[LNode;"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJNodeArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJNodeMatrixRegion
  (Node* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "[LNode;"), NULL);
	} else {
		jobjectArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv, "[LNode;"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJNodeArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}

Node* toNodeArray
  (jobjectArray arrayValue, jint arrayLength)
{
	Node* inArrayValue = (Node*) malloc(arrayLength * sizeof(Node));
	int          inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	jobject inNode;

	if (arrayLength < inArrayLength) {
		for (int k = 0; k < arrayLength; k++) {
			inNode = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toNode(inNode);
		}
	} else {
		for (int k = 0; k < inArrayLength; k++) {
			inNode = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toNode(inNode);
		}
		for (int k = inArrayLength; k < arrayLength; k++) {
			inArrayValue[k] = NodeNULL;
		}
	}
	return inArrayValue;
}

Node** toNodeMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    Node** inMatrixValue = (Node**) malloc(matrixRows * sizeof(Node*));
    int           inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
    if (matrixRows < inMatrixRows) {
    	for(int k = 0; k < matrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toNodeArray(inRow, matrixColumns);
    	}
    } else {
    	for(int k = 0; k < inMatrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toNodeArray(inRow, matrixColumns);
    	}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (Node*) malloc(matrixColumns * sizeof(Node));
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[k][h] = NodeNULL;
			}
		}
    }
	return inMatrixValue;
}

void setNodeArray
  (Node* inArrayValue, jobjectArray arrayValue, jint arrayLength)
{
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	jobject inNode;
	if (arrayLength < inArrayLength) {
		for (int k = 0; k < arrayLength; k++) {
			inNode = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toNode(inNode);
		}
	} else {
		for (int k = 0; k < inArrayLength; k++) {
			inNode = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toNode(inNode);
		}
	}
}

Node* toNodeMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    Node* inMatrixValue = (Node*) malloc(matrixRows * matrixColumns * sizeof(Node*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setNodeArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setNodeArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = NodeNULL;
				index++;
			}
		}
    }
	return inMatrixValue;
}

jobject newNodeObject
  (Node* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		return toJNode(*value);
	}
}

Node getNodeValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return NodeNULL;
    } else {
        return toNode(object);
    }
}

Node* NodePtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
	    return NULL;
    } else {
    	Node* ptr = (Node*) malloc(sizeof(Node));
	    *ptr = getNodeValue(object);
	    return ptr;
    }
}

void setNodePtrValue
  (Node* ptr, Node value)
{
	if (ptr != NULL) {
		*ptr = value;
	}
}

Node getNodePtrValue
  (Node* ptr)
{
	if (ptr == NULL) {
		return NodeNULL;
	} else {
		return *ptr;
	}
}

Node** toNodeMatrixPtr
  (Node* matrix, int rows, int columns)
{
	Node** matrixPtr = (Node**) malloc(rows * sizeof(Node*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (Node*) malloc(columns * sizeof(Node));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

Node* toNodeMatrixRegionPtr
  (Node** matrix, int rows, int columns)
{
	Node* matrixRegionPtr = (Node*) malloc(rows * columns * sizeof(Node));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setNodeMatrix
  (Node** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    int inMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
    if (matrixRows < inMatrixRows) {
    	for(int k = 0; k < matrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toNodeArray(inRow, matrixColumns);
    	}
    } else {
    	for(int k = 0; k < inMatrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toNodeArray(inRow, matrixColumns);
    	}
    }
}

void setNodeMatrixRegion
  (Node* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jobjectArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setNodeArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setNodeArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = NodeNULL;
					index++;
				}
			}
		}
	}
}

void setJNodeArray
  (jobjectArray outArray, Node* arrayValue, int* arrayLength)
{
	if (outArray != NULL && arrayValue != NULL && arrayLength != NULL) {
		jobject Node;
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			for (int k=0; k < outArrayLength; k++) {
			    Node = toJNode(arrayValue[k]);
			    (*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, Node);
			}
		} else {
			for (int k=0; k < *arrayLength; k++) {
				Node = toJNode(arrayValue[k]);
				(*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, Node);
			}
		}
	}
}

void setJNodeMatrix
  (jobjectArray matrix, Node** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jobjectArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJNodeArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJNodeArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJNodeMatrixRegion
  (jobjectArray matrix, Node* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jobjectArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJNodeArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJNodeArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}


//////////////////////////////////////////////////////////
//                  Assert operations                   //
//////////////////////////////////////////////////////////

void setAssertInfoNode_(const char* file, const char* function, int line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LTestResultPanel;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "setAssertInfo", "(Ljava/lang/String;Ljava/lang/String;I)V");
	(*javaEnv)->CallStaticIntMethod(javaEnv, cls, valueOf, toJstring(file), toJstring(function), toJint(line));
}

void assertEqualsObjectNode_(jobject expected, jobject actual) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertEquals", "(Ljava/lang/Object;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, expected, actual);
}

void assertEquals_Node_(Node expected, Node actual) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	assertEqualsObjectNode_(toJNode(expected), toJNode(actual));
}

void assertArrayEquals_Node_(Node* expected, int expectedLength, Node* actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJNodeArray(expected, &expectedLength), toJNodeArray(actual, &actualLength));
}

void assertMatrixEquals_Node_(Node** expected, int expectedRows, int expectedColumns, Node** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJNodeMatrix(expected, &expectedRows, &expectedColumns), toJNodeMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_Node_(Node* expected, int expectedRows, int expectedColumns, Node* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJNodeMatrixRegion(expected, &expectedRows, &expectedColumns), toJNodeMatrixRegion(actual, &actualRows, &actualColumns));
}

#endif
