/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _SimpleList_test_c
#define _SimpleList_test_c

#include "Node.h"
#include "Node_test.h"
#include "SimpleList.h"
#include "SimpleList_test.h"
#include "jni_test.h"

extern JNIEnv *javaEnv;

//////////////////////////////////////////////////////////
//         ObjectToStruct and StructToObject            //
//////////////////////////////////////////////////////////

jobject toJSimpleList
  (SimpleList value)
{
    jclass cls = (*javaEnv)->FindClass(javaEnv, "LSimpleList;");
    jmethodID constructor = (*javaEnv)->GetMethodID(javaEnv, cls, "<init>", "()V");
    jobject simpleList = (*javaEnv)->NewObject(javaEnv, cls, constructor);
    jfieldID numberFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "number", "I");
    (*javaEnv)->SetIntField(javaEnv, simpleList, numberFieldID, value.number);
    jfieldID firstFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "first", "LNode;");
    (*javaEnv)->SetObjectField(javaEnv, simpleList, firstFieldID, toJNodePtr(value.first));

    return simpleList;
}

SimpleList toSimpleList
  (jobject value)
{
    if ((*javaEnv)->IsSameObject(javaEnv, value, NULL)) {
        return SimpleListNULL;

    } else {
		jclass cls = (*javaEnv)->FindClass(javaEnv, "LSimpleList;");

		jfieldID numberFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "number", "I");
		jint number = (*javaEnv)->GetIntField(javaEnv, value, numberFieldID);

		jfieldID firstFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "first", "LNode;");
		jobject first = (*javaEnv)->GetObjectField(javaEnv, value, firstFieldID);

		SimpleList c_value;
		c_value.number = toInt(number);
		c_value.first  = toNodePtr(first);
		return c_value;
    }
}

void setSimpleListValue
  (jobject object, SimpleList* value)
{
    if (value != NULL && ! (*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
        jclass cls = (*javaEnv)->GetObjectClass(javaEnv, object);

        jfieldID numberFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "number", "I");
        (*javaEnv)->SetIntField(javaEnv, object, numberFieldID, value->number);

        jfieldID firstFieldID = (*javaEnv)->GetFieldID(javaEnv, cls, "first", "LNode;");
        (*javaEnv)->SetObjectField(javaEnv, object, firstFieldID, toJNodePtr(value->first));
    }
}


//////////////////////////////////////////////////////////
//                 Common operations                    //
//////////////////////////////////////////////////////////

jobjectArray toJSimpleListArray
  (SimpleList* arrayValue, int* arrayLength)
{
	if (arrayValue == NULL || arrayLength == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "LSimpleList;"), NULL);
	} else {
		jobjectArray outArray = (*javaEnv)->NewObjectArray(javaEnv, *arrayLength, (*javaEnv)->FindClass(javaEnv, "LSimpleList;"), NULL);
		jobject SimpleList;
		for (int k=0; k < *arrayLength; k++) {
			SimpleList = toJSimpleList(arrayValue[k]);
			(*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, SimpleList);
		}
		return outArray;
	}
}

jobjectArray toJSimpleListMatrix
  (SimpleList** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "[LSimpleList;"), NULL);
	} else {
		jobjectArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv, "[LSimpleList;"), NULL);
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJSimpleListArray(matrixValue[k], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
		}
		return matrix;
	}
}

jobjectArray toJSimpleListMatrixRegion
  (SimpleList* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue == NULL || matrixRows == NULL || matrixColumns == NULL) {
		return (*javaEnv)->NewObjectArray(javaEnv, 0, (*javaEnv)->FindClass(javaEnv, "[LSimpleList;"), NULL);
	} else {
		jobjectArray rowValue;
		jobjectArray matrix = (*javaEnv)->NewObjectArray(javaEnv, *matrixRows, (*javaEnv)->FindClass(javaEnv, "[LSimpleList;"), NULL);
		int index = 0;
		for(int k = 0; k < *matrixRows; k++) {
			rowValue = toJSimpleListArray(&matrixValue[index], matrixColumns);
			(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			index += *matrixColumns;
		}
		return matrix;
	}
}

SimpleList* toSimpleListArray
  (jobjectArray arrayValue, jint arrayLength)
{
	SimpleList* inArrayValue = (SimpleList*) malloc(arrayLength * sizeof(SimpleList));
	int          inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	jobject inSimpleList;

	if (arrayLength < inArrayLength) {
		for (int k = 0; k < arrayLength; k++) {
			inSimpleList = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toSimpleList(inSimpleList);
		}
	} else {
		for (int k = 0; k < inArrayLength; k++) {
			inSimpleList = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toSimpleList(inSimpleList);
		}
		for (int k = inArrayLength; k < arrayLength; k++) {
			inArrayValue[k] = SimpleListNULL;
		}
	}
	return inArrayValue;
}

SimpleList** toSimpleListMatrix
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    SimpleList** inMatrixValue = (SimpleList**) malloc(matrixRows * sizeof(SimpleList*));
    int           inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
    if (matrixRows < inMatrixRows) {
    	for(int k = 0; k < matrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toSimpleListArray(inRow, matrixColumns);
    	}
    } else {
    	for(int k = 0; k < inMatrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toSimpleListArray(inRow, matrixColumns);
    	}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			inMatrixValue[k] = (SimpleList*) malloc(matrixColumns * sizeof(SimpleList));
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[k][h] = SimpleListNULL;
			}
		}
    }
	return inMatrixValue;
}

void setSimpleListArray
  (SimpleList* inArrayValue, jobjectArray arrayValue, jint arrayLength)
{
	int inArrayLength = (*javaEnv)->GetArrayLength(javaEnv, arrayValue);
	jobject inSimpleList;
	if (arrayLength < inArrayLength) {
		for (int k = 0; k < arrayLength; k++) {
			inSimpleList = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toSimpleList(inSimpleList);
		}
	} else {
		for (int k = 0; k < inArrayLength; k++) {
			inSimpleList = (*javaEnv)->GetObjectArrayElement(javaEnv, arrayValue, k);
			inArrayValue[k] = toSimpleList(inSimpleList);
		}
	}
}

SimpleList* toSimpleListMatrixRegion
  (jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    SimpleList* inMatrixValue = (SimpleList*) malloc(matrixRows * matrixColumns * sizeof(SimpleList*));
    int  inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
	int index = 0;
    if (matrixRows < inMatrixRows) {
		for(int k = 0; k < matrixRows; k++) {
			inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setSimpleListArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
    } else {
		for(int k = 0; k < inMatrixRows; k++) {
			inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
			setSimpleListArray(&inMatrixValue[index], inRow, matrixColumns);
			index += (int) matrixColumns;
		}
		for(int k = inMatrixRows; k < matrixRows; k++) {
			for (int h = 0; h < matrixColumns; h++) {
				inMatrixValue[index] = SimpleListNULL;
				index++;
			}
		}
    }
	return inMatrixValue;
}

jobject newSimpleListObject
  (SimpleList* value)
{
	if (value == NULL) {
		return NULL;
	} else {
		return toJSimpleList(*value);
	}
}

SimpleList getSimpleListValue
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
    	return SimpleListNULL;
    } else {
        return toSimpleList(object);
    }
}

SimpleList* SimpleListPtr
  (jobject object)
{
    if ((*javaEnv)->IsSameObject(javaEnv, object, NULL)) {
	    return NULL;
    } else {
    	SimpleList* ptr = (SimpleList*) malloc(sizeof(SimpleList));
	    *ptr = getSimpleListValue(object);
	    return ptr;
    }
}

void setSimpleListPtrValue
  (SimpleList* ptr, SimpleList value)
{
	if (ptr != NULL) {
		*ptr = value;
	}
}

SimpleList getSimpleListPtrValue
  (SimpleList* ptr)
{
	if (ptr == NULL) {
		return SimpleListNULL;
	} else {
		return *ptr;
	}
}

SimpleList** toSimpleListMatrixPtr
  (SimpleList* matrix, int rows, int columns)
{
	SimpleList** matrixPtr = (SimpleList**) malloc(rows * sizeof(SimpleList*));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
    	matrixPtr[row] = (SimpleList*) malloc(columns * sizeof(SimpleList));
	    for (int column = 0; column < columns; column++ ) {
	    	matrixPtr[row][column] = matrix[index++];
	    }
    }
	return matrixPtr;
}

SimpleList* toSimpleListMatrixRegionPtr
  (SimpleList** matrix, int rows, int columns)
{
	SimpleList* matrixRegionPtr = (SimpleList*) malloc(rows * columns * sizeof(SimpleList));
	int index = 0;
    for (int row = 0; row < rows; row++ ) {
	    for (int column = 0; column < columns; column++ ) {
	    	matrixRegionPtr[index++] = matrix[row][column];
	    }
    }
	return matrixRegionPtr;
}

void setSimpleListMatrix
  (SimpleList** inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
    int inMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
    jobjectArray inRow;
    if (matrixRows < inMatrixRows) {
    	for(int k = 0; k < matrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toSimpleListArray(inRow, matrixColumns);
    	}
    } else {
    	for(int k = 0; k < inMatrixRows; k++) {
    		inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
    		inMatrixValue[k] = toSimpleListArray(inRow, matrixColumns);
    	}
    }
}

void setSimpleListMatrixRegion
  (SimpleList* inMatrixValue, jobjectArray matrixValue, jint matrixRows, jint matrixColumns)
{
	if (inMatrixValue != NULL && matrixRows > 0) {
		int inMatrixRows  = (*javaEnv)->GetArrayLength(javaEnv, matrixValue);
		jobjectArray inRow;
		int index = 0;
		if (matrixRows < inMatrixRows) {
			for(int k = 0; k < matrixRows; k++) {
				inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setSimpleListArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
		} else {
			for(int k = 0; k < inMatrixRows; k++) {
				inRow = (jobjectArray) (*javaEnv)->GetObjectArrayElement(javaEnv, matrixValue, k);
				setSimpleListArray(&inMatrixValue[index], inRow, matrixColumns);
				index += (int) matrixColumns;
			}
			for(int k = inMatrixRows; k < matrixRows; k++) {
				for (int h = 0; h < matrixColumns; h++) {
					inMatrixValue[index] = SimpleListNULL;
					index++;
				}
			}
		}
	}
}

void setJSimpleListArray
  (jobjectArray outArray, SimpleList* arrayValue, int* arrayLength)
{
	if (outArray != NULL && arrayValue != NULL && arrayLength != NULL) {
		jobject SimpleList;
		int outArrayLength = (*javaEnv)->GetArrayLength(javaEnv, outArray);
		if (outArrayLength < *arrayLength) {
			for (int k=0; k < outArrayLength; k++) {
			    SimpleList = toJSimpleList(arrayValue[k]);
			    (*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, SimpleList);
			}
		} else {
			for (int k=0; k < *arrayLength; k++) {
				SimpleList = toJSimpleList(arrayValue[k]);
				(*javaEnv)->SetObjectArrayElement(javaEnv, outArray, k, SimpleList);
			}
		}
	}
}

void setJSimpleListMatrix
  (jobjectArray matrix, SimpleList** matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL) {
		jobjectArray rowValue;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJSimpleListArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJSimpleListArray(matrixValue[k], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
			}
		}
	}
}

void setJSimpleListMatrixRegion
  (jobjectArray matrix, SimpleList* matrixValue, int* matrixRows, int* matrixColumns)
{
	if (matrixValue != NULL && matrixRows != NULL && matrixColumns != NULL) {
		jobjectArray rowValue;
		int index = 0;
		int outMatrixRows = (*javaEnv)->GetArrayLength(javaEnv, matrix);
		if (outMatrixRows < *matrixRows) {
			for(int k = 0; k < outMatrixRows; k++) {
				rowValue = toJSimpleListArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		} else {
			for(int k = 0; k < *matrixRows; k++) {
				rowValue = toJSimpleListArray(&matrixValue[index], matrixColumns);
				(*javaEnv)->SetObjectArrayElement(javaEnv, matrix, k, rowValue);
				index += *matrixColumns;
			}
		}
	}
}


//////////////////////////////////////////////////////////
//                  Assert operations                   //
//////////////////////////////////////////////////////////

void setAssertInfoSimpleList_(const char* file, const char* function, int line) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "LTestResultPanel;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "setAssertInfo", "(Ljava/lang/String;Ljava/lang/String;I)V");
	(*javaEnv)->CallStaticIntMethod(javaEnv, cls, valueOf, toJstring(file), toJstring(function), toJint(line));
}

void assertEqualsObjectSimpleList_(jobject expected, jobject actual) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertEquals", "(Ljava/lang/Object;Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, expected, actual);
}

void assertEquals_SimpleList_(SimpleList expected, SimpleList actual) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	assertEqualsObjectSimpleList_(toJSimpleList(expected), toJSimpleList(actual));
}

void assertArrayEquals_SimpleList_(SimpleList* expected, int expectedLength, SimpleList* actual, int actualLength) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJSimpleListArray(expected, &expectedLength), toJSimpleListArray(actual, &actualLength));
}

void assertMatrixEquals_SimpleList_(SimpleList** expected, int expectedRows, int expectedColumns, SimpleList** actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJSimpleListMatrix(expected, &expectedRows, &expectedColumns), toJSimpleListMatrix(actual, &actualRows, &actualColumns));
}

void assertMatrixRegionEquals_SimpleList_(SimpleList* expected, int expectedRows, int expectedColumns, SimpleList* actual, int actualRows, int actualColumns) {
	if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;
	jclass cls = (*javaEnv)->FindClass(javaEnv, "Lorg/junit/Assert;");
	jmethodID valueOf = (*javaEnv)->GetStaticMethodID(javaEnv, cls, "assertArrayEquals", "([Ljava/lang/Object;[Ljava/lang/Object;)V");
	(*javaEnv)->CallStaticObjectMethod(javaEnv, cls, valueOf, toJSimpleListMatrixRegion(expected, &expectedRows, &expectedColumns), toJSimpleListMatrixRegion(actual, &actualRows, &actualColumns));
}

#endif
