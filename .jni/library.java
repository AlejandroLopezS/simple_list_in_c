/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

public class library {
    @executerpane.MethodAnnotation(signature = "addAtFirst(char,SimpleList):SimpleList")
    public SimpleList addAtFirst(char value, SimpleList list){
        return addAtFirst_(value, list);
    }
    private native SimpleList addAtFirst_(char value, SimpleList list);

    @executerpane.MethodAnnotation(signature = "addAtIndex(char,int,SimpleList):SimpleList")
    public SimpleList addAtIndex(char value, int index, SimpleList list){
        return addAtIndex_(value, index, list);
    }
    private native SimpleList addAtIndex_(char value, int index, SimpleList list);

    @executerpane.MethodAnnotation(signature = "addAtLast(char,SimpleList):SimpleList")
    public SimpleList addAtLast(char value, SimpleList list){
        return addAtLast_(value, list);
    }
    private native SimpleList addAtLast_(char value, SimpleList list);

    @executerpane.MethodAnnotation(signature = "indexOf(char,SimpleList):int")
    public int indexOf(char value, SimpleList list){
        return indexOf_(value, list);
    }
    private native int indexOf_(char value, SimpleList list);

    @executerpane.MethodAnnotation(signature = "removeAtFirst(SimpleList):SimpleList")
    public SimpleList removeAtFirst(SimpleList list){
        return removeAtFirst_(list);
    }
    private native SimpleList removeAtFirst_(SimpleList list);

    @executerpane.MethodAnnotation(signature = "removeAtIndex(int,SimpleList):SimpleList")
    public SimpleList removeAtIndex(int index, SimpleList list){
        return removeAtIndex_(index, list);
    }
    private native SimpleList removeAtIndex_(int index, SimpleList list);

    @executerpane.MethodAnnotation(signature = "removeAtLast(SimpleList):SimpleList")
    public SimpleList removeAtLast(SimpleList list){
        return removeAtLast_(list);
    }
    private native SimpleList removeAtLast_(SimpleList list);

    @executerpane.MethodAnnotation(signature = "removeValue(char,SimpleList):SimpleList")
    public SimpleList removeValue(char value, SimpleList list){
        return removeValue_(value, list);
    }
    private native SimpleList removeValue_(char value, SimpleList list);

    @executerpane.MethodAnnotation(signature = "valueAtIndex(int,SimpleList):char")
    public char valueAtIndex(int index, SimpleList list){
        return valueAtIndex_(index, list);
    }
    private native char valueAtIndex_(int index, SimpleList list);


    static {
        System.load(new java.io.File(".jni", "library_jni.so").getAbsolutePath());
    }
}
