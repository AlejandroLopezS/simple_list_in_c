#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "jni_inout.h"
#include "SimpleList_test.h"
#include "SimpleList.h"
#include "library.c"
extern JNIEnv *javaEnv;

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

JNIEXPORT jobject JNICALL Java_library_addAtFirst_1
  (JNIEnv *env, jobject object, jchar value, jobject list)
{
    javaEnv = env;
    char c_value = toChar(value);
    SimpleList c_list = toSimpleList(list);
    SimpleList c_outValue = addAtFirst(c_value, c_list);
    return toJSimpleList(c_outValue);
}

JNIEXPORT jobject JNICALL Java_library_addAtIndex_1
  (JNIEnv *env, jobject object, jchar value, jint index, jobject list)
{
    javaEnv = env;
    char c_value = toChar(value);
    int c_index = toInt(index);
    SimpleList c_list = toSimpleList(list);
    SimpleList c_outValue = addAtIndex(c_value, c_index, c_list);
    return toJSimpleList(c_outValue);
}

JNIEXPORT jobject JNICALL Java_library_addAtLast_1
  (JNIEnv *env, jobject object, jchar value, jobject list)
{
    javaEnv = env;
    char c_value = toChar(value);
    SimpleList c_list = toSimpleList(list);
    SimpleList c_outValue = addAtLast(c_value, c_list);
    return toJSimpleList(c_outValue);
}

JNIEXPORT jint JNICALL Java_library_indexOf_1
  (JNIEnv *env, jobject object, jchar value, jobject list)
{
    javaEnv = env;
    char c_value = toChar(value);
    SimpleList c_list = toSimpleList(list);
    int c_outValue = indexOf(c_value, c_list);
    return toJint(c_outValue);
}

JNIEXPORT jobject JNICALL Java_library_removeAtFirst_1
  (JNIEnv *env, jobject object, jobject list)
{
    javaEnv = env;
    SimpleList c_list = toSimpleList(list);
    SimpleList c_outValue = removeAtFirst(c_list);
    return toJSimpleList(c_outValue);
}

JNIEXPORT jobject JNICALL Java_library_removeAtIndex_1
  (JNIEnv *env, jobject object, jint index, jobject list)
{
    javaEnv = env;
    int c_index = toInt(index);
    SimpleList c_list = toSimpleList(list);
    SimpleList c_outValue = removeAtIndex(c_index, c_list);
    return toJSimpleList(c_outValue);
}

JNIEXPORT jobject JNICALL Java_library_removeAtLast_1
  (JNIEnv *env, jobject object, jobject list)
{
    javaEnv = env;
    SimpleList c_list = toSimpleList(list);
    SimpleList c_outValue = removeAtLast(c_list);
    return toJSimpleList(c_outValue);
}

JNIEXPORT jobject JNICALL Java_library_removeValue_1
  (JNIEnv *env, jobject object, jchar value, jobject list)
{
    javaEnv = env;
    char c_value = toChar(value);
    SimpleList c_list = toSimpleList(list);
    SimpleList c_outValue = removeValue(c_value, c_list);
    return toJSimpleList(c_outValue);
}

JNIEXPORT jchar JNICALL Java_library_valueAtIndex_1
  (JNIEnv *env, jobject object, jint index, jobject list)
{
    javaEnv = env;
    int c_index = toInt(index);
    SimpleList c_list = toSimpleList(list);
    char c_outValue = valueAtIndex(c_index, c_list);
    return toJchar(c_outValue);
}

