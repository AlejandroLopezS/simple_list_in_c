public class SimpleList {
    private int  number;
    private Node first;

    @executerpane.ConstructorAnnotation(signature = "SimpleListNULL")
    public SimpleList() {
        number = 0;
        first  = null;
    }
    
    @executerpane.ConstructorAnnotation(signature = "SimpleList(int[])")
    public SimpleList(char[] value) {
       number = value.length;
       first = new Node(value);
    }
    
    
    public void addAtFirst(char c) {
    	if (first == null) {
    		first = new Node(c);
    	} else {
    		first.addAtFirst(c);
    	}
    	number++;
    }
    
    public void addAtLast(char c) {
    	if (first == null) {
    		first = new Node(c);
    	} else {
    		first.addAtLast(c);
    	}
    	number++;
    }
    
    @Override
	public String toString() {
    	if (first == null) {
    		return "empty list";
    	} else {
    		return first.toString();
    	}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SimpleList other = (SimpleList) obj;
		if (number != other.number) {
			return false;
		}
		if (first == null) {
			return other.first == null;
		} else {
			return first.equals(other.first);
		}
	}
}