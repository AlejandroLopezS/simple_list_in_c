/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

#ifndef _SimpleList_test_h
#define _SimpleList_test_h

#define assertEquals_SimpleList(expected, actual)                                                                       if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoSimpleList_(__FILE__, __FUNCTION__, __LINE__);assertEquals_SimpleList_(expected, actual)
#define assertArrayEquals_SimpleList(expected, expectedLength, actual, actualLength)                                    if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoSimpleList_(__FILE__, __FUNCTION__, __LINE__);assertArrayEquals_SimpleList_(expected, expectedLength, actual, actualLength)
#define assertMatrixEquals_SimpleList(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)       if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoSimpleList_(__FILE__, __FUNCTION__, __LINE__);assertMatrixEquals_SimpleList_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)
#define assertMatrixRegionEquals_SimpleList(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns) if ((*javaEnv)->ExceptionOccurred(javaEnv)) return;setAssertInfoSimpleList_(__FILE__, __FUNCTION__, __LINE__);assertMatrixRegionEquals_SimpleList_(expected, expectedRows, expectedColumns, actual, actualRows, actualColumns)

#include "SimpleList_test.c"
#endif
