#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "jni_test.h"
#include "SimpleList_test.h"
#include "SimpleList.h"
#include "library.c"

/**
 * @author Francisco Guerra (francisco.guerra@ulpgc.es)
 * @version 1.0
 */

///////////////////////////////////////////////
//                 IndexOf                   //
///////////////////////////////////////////////
void testIndexOf_A() {
SimpleList list = {1, newNode('i', NULL)};
    /* Node node = (Node) malloc(sizeof(Node));
    node->item = 'B';
    node->next = NULL;
    SimpleList list = {1, node};
    */
    int index = indexOf('i', list);
    assertEquals_int(0, index);
}
void testIndexOf_B() {
    SimpleList list = {1, newNode('w', NULL)};
    /* Node node = (Node) malloc(sizeof(Node));
    node->item = 'B';
    node->next = NULL;
    SimpleList list = {1, node};
    */
    int index = indexOf('I', list);
    assertEquals_int(-1, index);
}

void testIndexOf_C() {
    SimpleList list = {3, newNode('i', newNode('D', newNode('I', NULL)))};
    /* Node node = (Node) malloc(sizeof(Node));
    node->item = 'B';
    node->next = NULL;
    SimpleList list = {1, node};
    */
    int index = indexOf('D', list);
    assertEquals_int(1, index);
}

void testIndexOf_D() {
    SimpleList list = {4, newNode('D', newNode('I',newNode('R',newNode('i',NULL))))};
    /* Node node = (Node) malloc(sizeof(Node));
    node->item = 'B';
    node->next = NULL;
    SimpleList list = {1, node};
    */
    int index = indexOf('R', list);
    assertEquals_int(2, index);
}

void testIndexOf_E() {
    SimpleList list = {5, newNode('w', newNode('D',newNode('i',newNode('I',newNode('B', NULL)))))};
    /* Node node = (Node) malloc(sizeof(Node));
    node->item = 'B';
    node->next = NULL;
    SimpleList list = {1, node};
    */
    int index = indexOf('B', list);
    assertEquals_int(4, index);
}

///////////////////////////////////////////////
//               ValueAtIndex                //
///////////////////////////////////////////////
void testValueAtIndex_A() {
    // Given
    SimpleList list = {NULL};
    // When
    char index = valueAtIndex(0, list);
    // Then
    assertEquals_char('!', index);
}

void testValueAtIndex_B() {
    // Given
    SimpleList list = {1, newNode('i', NULL)};
    // When
    char index = valueAtIndex(0, list);
    // Then
    assertEquals_char('i', index);
}

void testValueAtIndex_C() {
    // Given
    SimpleList list = {3, newNode('i', newNode('w', newNode('I', NULL)))};
    // When
    char index = valueAtIndex(1, list);
    // Then
    assertEquals_char('w', index);
}

void testValueAtIndex_D() {
    // Given
    SimpleList list = {3, newNode('i', newNode('w', newNode('I', newNode('D', NULL))))};
    // When
    char index = valueAtIndex(2, list);
    // Then
    assertEquals_char('I', index);
}

void testValueAtIndex_E() {
    // Given
    SimpleList list = {3, newNode('i', newNode('w', newNode('I', newNode('D', newNode('R', NULL)))))};
    // When
    char index = valueAtIndex(4, list);
    // Then
    assertEquals_char('R', index);
}

///////////////////////////////////////////////
//               AddAtFirst                  //
///////////////////////////////////////////////
void testAddAtFirst_A() {
    // Given
    SimpleList list = {NULL};
    SimpleList list1 = {1, newNode('i', NULL)};
    // When
    SimpleList resultado = addAtFirst('i', list);
    // Then
    assertEquals_SimpleList(list1, resultado);
}

void testAddAtFirst_B() {
    SimpleList list = {1, newNode('w', NULL)};
    SimpleList list1 = {2, newNode('I', newNode('w', NULL))};
    // When
    SimpleList resultado = addAtFirst('I', list);
    // Then
    assertEquals_SimpleList(list1, resultado);
}

void testAddAtFirst_C() {
    SimpleList list = {3,  newNode('i', newNode('w', newNode('I', NULL)))};
    SimpleList list1 = {4,  newNode('D', newNode('i', newNode('w', newNode('I', NULL))))};
    // When
    SimpleList resultado = addAtFirst('D', list);
    // Then
    assertEquals_SimpleList(list1, resultado);
}

void testAddAtFirst_D() {
    SimpleList list = {4, newNode('D', newNode('I', newNode('w', newNode('i', NULL))))};
    SimpleList list1 = {5, newNode('R', newNode('D', newNode('I', newNode('w', newNode('i', NULL)))))};
    // When
    SimpleList resultado = addAtFirst('R', list);
    // Then
    assertEquals_SimpleList(list1, resultado);
}

void testAddAtFirst_E() {
    SimpleList list = {5, newNode('w', newNode('D', newNode('i', newNode('I', newNode('R', NULL)))))};
    SimpleList list1 = {6, newNode('B', newNode('w', newNode('D', newNode('i', newNode('I', newNode('R', NULL))))))};
    // When
    SimpleList resultado = addAtFirst('B', list);
    // Then
    assertEquals_SimpleList(list1, resultado);
}

///////////////////////////////////////////////
//              RemoveAtFirst                //
///////////////////////////////////////////////
void testRemoveAtFirst_A() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtFirst_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtFirst_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtFirst_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtFirst_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

///////////////////////////////////////////////
//                AddAtLast                  //
///////////////////////////////////////////////
void testAddAtLast_A() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testAddAtLast_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testAddAtLast_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testAddAtLast_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testAddAtLast_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

///////////////////////////////////////////////
//               RemoveAtLast                //
///////////////////////////////////////////////
void testRemoveAtLast_A() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtLast_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtLast_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtLast_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtLast_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

///////////////////////////////////////////////
//               AddAtIndex                  //
///////////////////////////////////////////////
void testAddAtIndex_A() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testAddAtIndex_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testAddAtIndex_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testAddAtIndex_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testAddAtIndex_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

///////////////////////////////////////////////
//              RemoveAtIndex                //
///////////////////////////////////////////////
void testRemoveAtIndex_A() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtIndex_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtIndex_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtIndex_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveAtIndex_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

///////////////////////////////////////////////
//               RemoveValue                 //
///////////////////////////////////////////////
void testRemoveValue_A() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveValue_B() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveValue_C() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveValue_D() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}

void testRemoveValue_E() {
    // Given
    
    // When
    
    // Then
    fail("Not yet implemented");
}
